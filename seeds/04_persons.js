
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('persons').truncate()
    .then(function () {
      // Inserts seed entries
      return knex('persons').insert([
        {id: 1, name: 'hienlt', age: '22', gender: 'female'},
        {id: 2, name: 'hanght', age: '22', gender: 'female'},
        {id: 3, name: 'thaontp', age: '22', gender: 'female'},
        {id: 4, name: 'dungnd', age: '22', gender: 'male'},
        {id: 5, name: 'dungnt', age: '22', gender: 'male'},
        {id: 6, name: 'haontp', age: '23', gender: 'female'}
      ]);
    });
};
