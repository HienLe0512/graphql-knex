import { knex } from '../connectors';
import * as promise from 'bluebird';

class Person {
    getAllPersons() {
        return knex('Persons').select();
    }

    createPerson(input: object) {
        if (input) {
            return knex('Persons').insert(input).then(function(result) {
                return knex('Persons').where('id', result[0]).first();
            });
        }
        return false;
    }

    deletePerson(id: number) {
        if (id) {
            return knex('Persons').where('id', id).del();
        }
        return false;
    }

    updatePerson(id: number, input: object) {
        if (id) {
            return knex('Persons').where('id', id).update(input);
        }
        return false;
    }

    insertPersons(Persons: object[]) {
        return knex.transaction((trans) => {
            promise.map(Persons, (Person) => {
                return knex.insert(Person).into('Persons').transacting(trans);
            })
            .then(trans.commit)
            .catch(trans.rollback);
          })
          .then(function(inserts) {
            console.log(inserts.length);
          })
          .catch(function(error) {
            console.log("rollback db");
            console.error(error);
            return false;
          });
    }

    findPerson(id: number) {
            return knex('Persons').where('id', id).first();
    }
}


export const PersonModel = new Person();